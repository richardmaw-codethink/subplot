use subplotlib::prelude::*;

// --------------------------------

lazy_static! {
    static ref SUBPLOT_EMBEDDED_FILES: Vec<SubplotDataFile> =
        vec![SubplotDataFile::new("aGVsbG8udHh0", "aGVsbG8sIHdvcmxkCg=="),];
}

// ---------------------------------

// Create on-disk files from embedded files
#[test]
fn create_on_disk_files_from_embedded_files() {
    let mut scenario = Scenario::new(&base64_decode(
        "Q3JlYXRlIG9uLWRpc2sgZmlsZXMgZnJvbSBlbWJlZGRlZCBmaWxlcw==",
    ));

    let step = subplotlib::steplibrary::files::create_from_embedded::Builder::default()
        .embedded_file({
            use std::path::PathBuf;
            // hello.txt
            let target_name: PathBuf = base64_decode("aGVsbG8udHh0").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_exists::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_contains::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .data(
            // "hello, world"
            &base64_decode("aGVsbG8sIHdvcmxk"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_does_not_exist::Builder::default()
        .filename(
            // "other.txt"
            &base64_decode("b3RoZXIudHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step =
        subplotlib::steplibrary::files::create_from_embedded_with_other_name::Builder::default()
            .filename_on_disk(
                // "other.txt"
                &base64_decode("b3RoZXIudHh0"),
            )
            .embedded_file({
                use std::path::PathBuf;
                // hello.txt
                let target_name: PathBuf = base64_decode("aGVsbG8udHh0").into();
                SUBPLOT_EMBEDDED_FILES
                    .iter()
                    .find(|df| df.name() == target_name)
                    .expect("Unable to find file at runtime")
                    .clone()
            })
            .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_exists::Builder::default()
        .filename(
            // "other.txt"
            &base64_decode("b3RoZXIudHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_match::Builder::default()
        .filename1(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .filename2(
            // "other.txt"
            &base64_decode("b3RoZXIudHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::only_these_exist::Builder::default()
        .filenames(
            // "hello.txt, other.txt"
            &base64_decode("aGVsbG8udHh0LCBvdGhlci50eHQ="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// File metadata
#[test]
fn file_metadata() {
    let mut scenario = Scenario::new(&base64_decode("RmlsZSBtZXRhZGF0YQ=="));

    let step = subplotlib::steplibrary::files::create_from_embedded::Builder::default()
        .embedded_file({
            use std::path::PathBuf;
            // hello.txt
            let target_name: PathBuf = base64_decode("aGVsbG8udHh0").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::remember_metadata::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::has_remembered_metadata::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::create_from_text::Builder::default()
        .text(
            // "yo"
            &base64_decode("eW8="),
        )
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::has_different_metadata::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// File modification time
#[test]
fn file_modification_time() {
    let mut scenario = Scenario::new(&base64_decode("RmlsZSBtb2RpZmljYXRpb24gdGltZQ=="));

    let step = subplotlib::steplibrary::files::touch_with_timestamp::Builder::default()
        .filename(
            // "foo.dat"
            &base64_decode("Zm9vLmRhdA=="),
        )
        .mtime(
            // "1970-01-02 03:04:05"
            &base64_decode("MTk3MC0wMS0wMiAwMzowNDowNQ=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::mtime_is_ancient::Builder::default()
        .filename(
            // "foo.dat"
            &base64_decode("Zm9vLmRhdA=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::touch::Builder::default()
        .filename(
            // "foo.dat"
            &base64_decode("Zm9vLmRhdA=="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::mtime_is_recent::Builder::default()
        .filename(
            // "foo.dat"
            &base64_decode("Zm9vLmRhdA=="),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// File contents
#[test]
fn file_contents() {
    let mut scenario = Scenario::new(&base64_decode("RmlsZSBjb250ZW50cw=="));

    let step = subplotlib::steplibrary::files::create_from_embedded::Builder::default()
        .embedded_file({
            use std::path::PathBuf;
            // hello.txt
            let target_name: PathBuf = base64_decode("aGVsbG8udHh0").into();
            SUBPLOT_EMBEDDED_FILES
                .iter()
                .find(|df| df.name() == target_name)
                .expect("Unable to find file at runtime")
                .clone()
        })
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_contains::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .data(
            // "hello, world"
            &base64_decode("aGVsbG8sIHdvcmxk"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_matches_regex::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .regex(
            // "hello, .*"
            &base64_decode("aGVsbG8sIC4q"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::file_matches_regex::Builder::default()
        .filename(
            // "hello.txt"
            &base64_decode("aGVsbG8udHh0"),
        )
        .regex(
            // "hello, .*"
            &base64_decode("aGVsbG8sIC4q"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}

// ---------------------------------

// Directories
#[test]
fn directories() {
    let mut scenario = Scenario::new(&base64_decode("RGlyZWN0b3JpZXM="));

    let step = subplotlib::steplibrary::files::make_directory::Builder::default()
        .path(
            // "first"
            &base64_decode("Zmlyc3Q="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_exists::Builder::default()
        .path(
            // "first"
            &base64_decode("Zmlyc3Q="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_is_empty::Builder::default()
        .path(
            // "first"
            &base64_decode("Zmlyc3Q="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_does_not_exist::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::remove_directory::Builder::default()
        .path(
            // "first"
            &base64_decode("Zmlyc3Q="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_does_not_exist::Builder::default()
        .path(
            // "first"
            &base64_decode("Zmlyc3Q="),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::make_directory::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_exists::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_is_empty::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    let step =
        subplotlib::steplibrary::files::create_from_embedded_with_other_name::Builder::default()
            .filename_on_disk(
                // "second/third/hello.txt"
                &base64_decode("c2Vjb25kL3RoaXJkL2hlbGxvLnR4dA=="),
            )
            .embedded_file({
                use std::path::PathBuf;
                // hello.txt
                let target_name: PathBuf = base64_decode("aGVsbG8udHh0").into();
                SUBPLOT_EMBEDDED_FILES
                    .iter()
                    .find(|df| df.name() == target_name)
                    .expect("Unable to find file at runtime")
                    .clone()
            })
            .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_is_not_empty::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_exists::Builder::default()
        .path(
            // "second/third"
            &base64_decode("c2Vjb25kL3RoaXJk"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_is_not_empty::Builder::default()
        .path(
            // "second/third"
            &base64_decode("c2Vjb25kL3RoaXJk"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::remove_directory::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    let step = subplotlib::steplibrary::files::path_does_not_exist::Builder::default()
        .path(
            // "second"
            &base64_decode("c2Vjb25k"),
        )
        .build();
    scenario.add_step(step, None);

    scenario.run().unwrap();
}
