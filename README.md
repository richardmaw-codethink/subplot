---
title: Subplot README
...

Capture and communicate acceptance criteria for software and systems,
and how they are verified, in a way that's understood by all project
stakeholders.

Acceptance criteria are expressed as _scenarios_ in the Cucumber
given/when/then style:

> given a web site subplot.liw.fi  
> when I retrieve the site front page  
> then it contains "Subplot"  
> and it contains "acceptance criteria"


## When all stakeholders really need to understand acceptance criteria

Subplot is a set of tools for specifying, documenting, and
implementing automated acceptance tests for systems and software.
Subplot tools aim to produce a human-readable document of acceptance
criteria and a program that automatically tests a system against those
criteria. The goal is for every stakeholder in a project to understand
the project’s acceptance criteria and how they’re verified.

See <https://subplot.liw.fi/> for the home page.

## Hacking Subplot

Subplot is written using the Rust programming language, so the usual
workflow for Rust applies. To build run `cargo build`, to run tests in
Rust run `cargo test`. You probably need to install Rust using rustup:
the version packaged in a Linux distribution is likely too old.

To run the whole test suite, including testing all examples and
Subplot self tests, run `./check` at the root of the source tree.

You'll need to install build dependencies installed. On a system
running Debian or a derivative of it:

~~~sh
$ sudo apt-get install build-essential git debhelper dh-cargo python3 \
  pandoc texlive-latex-base texlive-latex-recommended \
  texlive-fonts-recommended librsvg2-bin graphviz pandoc-citeproc \
  plantuml daemonize lmodern procps
~~~

Additionally, any packages reported by running the following command:

~~~sh
$ dpkg-checkbuilddeps
~~~

To build the Debian package:

~~~sh
$ git archive HEAD | gzip > "../subplot_$(dpkg-parsechangelog -SVersion | sed 's/-.*$//').orig.tar.gz"
$ dpkg-buildpackage -us -uc
~~~

# Legalese

Copyright 2019-2021  Lars Wirzenius, Daniel Silverstone

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
