
# Introduction

This document describes how we guard against accidental breaking
changes in Subplot by running it against a curated set of subplot
documents.

# Subplot

## Produce a PDF

~~~scenario
given an installed subplot
given a clone of https://gitlab.com/subplot/subplot.git in src at 725775084f47cc2449d8d55687cf163f6b8f2d96
when I docgen subplot.md to test.pdf, in src
then file src/test.pdf exists
~~~

## Produce HTML page

~~~scenario
given an installed subplot
given a clone of https://gitlab.com/subplot/subplot.git in src at 725775084f47cc2449d8d55687cf163f6b8f2d96
when I docgen subplot.md to test.html, in src
when I run, in src, subplot docgen subplot.md -o subplot.html
then file src/test.html exists
~~~

## Generate and run test program

~~~scenario
given an installed subplot
given file run_test.sh
given a clone of https://gitlab.com/subplot/subplot.git in src at 725775084f47cc2449d8d55687cf163f6b8f2d96
when I run, in src, subplot codegen subplot.md -o test-inner.py
when I run bash run_test.sh
then command is successful
~~~

~~~{#run_test.sh .file .sh}
#!/bin/bash

set -euo pipefail

N=100

if python3 src/test-inner.py --log test-inner.log --env "PATH=$PATH" --env SUBPLOT_DIR=/
then
    exit=0
else
    exit="$?"
fi

if [ "$exit" != 0 ]
then
    # Failure. Show end of inner log file.

    echo "last $N lines of test-inner.log:"
    tail "-n$N" test-inner.log | sed 's/^/    /'
fi

exit "$exit"
~~~


---
title: Test Subplot against reference subplots
author: The Subplot project
template: python
bindings:
- reference.yaml
- subplot.yaml
- lib/runcmd.yaml
- lib/files.yaml
functions:
- reference.py
- subplot.py
- lib/files.py
- lib/runcmd.py
...
