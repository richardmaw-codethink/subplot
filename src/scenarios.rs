use crate::ScenarioStep;
use serde::{Deserialize, Serialize};

/// An acceptance test scenario.
///
/// A scenario consists of a title, by which it can be identified, and
/// a sequence of steps. The Scenario struct assumes the steps are
/// valid and make sense; the struct does not try to validate the
/// sequence.
#[derive(Debug, Serialize, Deserialize)]
pub struct Scenario {
    title: String,
    name: Option<String>,
    steps: Vec<ScenarioStep>,
}

impl Scenario {
    /// Construct a new scenario.
    ///
    /// The new scenario will have a title, but no steps.
    pub fn new(title: &str) -> Scenario {
        Scenario {
            title: title.to_string(),
            name: None,
            steps: vec![],
        }
    }

    /// Return the title of a scenario.
    pub fn title(&self) -> &str {
        &self.title
    }

    /// Set name of scenario.
    pub fn set_name(&mut self, name: &str) {
        self.name = Some(name.to_string());
    }

    /// Return name of scenario.
    pub fn name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    /// Does the scenario have steps?
    pub fn has_steps(&self) -> bool {
        !self.steps.is_empty()
    }

    /// Return slice with all the steps.
    pub fn steps(&self) -> &[ScenarioStep] {
        &self.steps
    }

    /// Add a step to a scenario.
    pub fn add(&mut self, step: &ScenarioStep) {
        self.steps.push(step.clone());
    }
}

#[cfg(test)]
mod test {
    use super::Scenario;
    use crate::ScenarioStep;
    use crate::StepKind;

    #[test]
    fn has_title() {
        let scen = Scenario::new("title");
        assert_eq!(scen.title(), "title");
    }

    #[test]
    fn has_no_name_initially() {
        let scen = Scenario::new("title");
        assert_eq!(scen.name(), None);
    }

    #[test]
    fn sets_name() {
        let mut scen = Scenario::new("title");
        scen.set_name("Alfred");
        assert_eq!(scen.name(), Some("Alfred"));
    }

    #[test]
    fn has_no_steps_initially() {
        let scen = Scenario::new("title");
        assert_eq!(scen.steps().len(), 0);
    }

    #[test]
    fn adds_step() {
        let mut scen = Scenario::new("title");
        let step = ScenarioStep::new(StepKind::Given, "and", "foo");
        scen.add(&step);
        assert_eq!(scen.steps(), &[step]);
    }
}
