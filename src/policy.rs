use crate::{Result, SubplotError};

use std::path::{Path, PathBuf};

/// Get the base directory given the name of the markdown file.
///
/// All relative filename, such as bindings files, are resolved
/// against the base directory.
pub fn get_basedir_from(filename: &Path) -> Result<PathBuf> {
    let dirname = match filename.parent() {
        None => return Err(SubplotError::BasedirError(filename.to_path_buf())),
        Some(x) => x.to_path_buf(),
    };
    Ok(dirname)
}
