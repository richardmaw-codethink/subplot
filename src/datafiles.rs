use pandoc_ast::{MutVisitor, Pandoc};
use serde::{Deserialize, Serialize};

/// A data file embedded in the document.
#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct DataFile {
    filename: String,
    contents: String,
}

impl DataFile {
    /// Create a new data file, with a name and contents.
    pub fn new(filename: String, contents: String) -> DataFile {
        DataFile { filename, contents }
    }

    /// Return name of embedded file.
    pub fn filename(&self) -> &str {
        &self.filename
    }

    /// Return contents of embedded file.
    pub fn contents(&self) -> &str {
        &self.contents
    }
}

/// A collection of data files embedded in document.
#[derive(Debug, Eq, PartialEq, Clone, Serialize, Deserialize)]
pub struct DataFiles {
    files: Vec<DataFile>,
}

impl DataFiles {
    /// Create new set of data files.
    pub fn new(ast: &mut Pandoc) -> DataFiles {
        let mut files = DataFiles { files: vec![] };
        files.walk_pandoc(ast);
        files
    }

    /// Return slice of all data files.
    pub fn files(&self) -> &[DataFile] {
        &self.files
    }

    /// Append a new data file.
    pub fn push(&mut self, file: DataFile) {
        self.files.push(file);
    }
}
